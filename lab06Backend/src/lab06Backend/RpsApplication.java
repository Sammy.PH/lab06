package lab06Backend;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import lab06Backend.backend.*;


public class RpsApplication extends Application {
	private RpsGames game = new RpsGames();
	public void start(Stage stage) {
		
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
		
		//buttons
		HBox buttons = new HBox();
		Button rock = new Button("Rock");
		Button paper = new Button("Paper");
		Button scissors = new Button("Scissors");
		buttons.getChildren().addAll(rock, paper, scissors);
		
		//textfields
		HBox textField = new HBox();
		TextField welcome = new TextField("Welcome");
		TextField numWins = new TextField("wins: ");
		TextField numLosses = new TextField("losses: ");
		TextField numTies = new TextField("ties: ");
		welcome.setPrefWidth(200);
		numWins.setPrefWidth(200);
		numLosses.setPrefWidth(200);
		numTies.setPrefWidth(200);
		textField.getChildren().addAll(welcome, numWins, numLosses, numTies);
		
		//adding buttons and textfields to root
		VBox container = new VBox();
		container.getChildren().addAll(buttons, textField);
		root.getChildren().addAll(container);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
} 
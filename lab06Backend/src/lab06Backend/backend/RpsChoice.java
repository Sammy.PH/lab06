package lab06Backend.backend;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class RpsChoice implements EventHandler<ActionEvent>{
	private TextField message;
	private TextField wins;
	private TextField ties;
	private TextField losses;
	private String choice;
	private RpsGames object;
	
	public RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, String choice, RpsGames object) {
		this.object = object;
		this.message = message;
		this.choice = choice;
		this.wins = wins;
		this.losses = losses;
		this.ties = ties;
	}
	
	@Override
	public void handle(ActionEvent e) {
	
		
	}
}

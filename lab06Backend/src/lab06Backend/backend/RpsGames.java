package lab06Backend.backend;
import java.util.*;
public class RpsGames {
	private int wins;
	private int ties;
	private int losses;
	private Random number;
	
	public RpsGames() {
		this.wins = 0;
		this.ties = 0;
		this.losses = 0;
		this.number = new Random();
	}
	
	public int getWins() {
		return this.wins;
	}
	
	public int getTies() {
		return this.ties;
	}
	
	public int getLosses() {
		return this.losses;
	}
	
	public String playRound(String playerChoice) {
		int symbol = number.nextInt(3);
		String choice;
		String winner;
		if(symbol == 0 && playerChoice.equals("rock")) {
			this.ties++;
			winner = "no one";
			choice = "rock";
		}
		else if(symbol == 0 && playerChoice.equals("scissors")) {
			this.wins++;
			winner = "player";
			choice = "rock";
		}
		else if(symbol == 0 && playerChoice.equals("paper")) {
			this.losses++;
			winner = "computer";
			choice = "rock";
		}
		else if(symbol == 1 && playerChoice.equals("rock")) {
			this.losses++;
			winner = "computer";
			choice = "scissors";
		}
		else if(symbol == 1 && playerChoice.equals("paper")) {
			this.wins++;
			winner = "player";
			choice = "scissors";
		}
		else if(symbol == 1 && playerChoice.equals("scissors")) {
			this.ties++;
			winner = "no one";
			choice = "scissors";
		}
		else if(symbol == 2 && playerChoice.equals("rock")) {
			this.wins++;
			winner = "player";
			choice = "paper";
		}
		else if(symbol == 2 && playerChoice.equals("paper")) {
			this.ties++;
			winner = "no one";
			choice = "paper";
		}
		else {
			this.losses++;
			winner = "computer";
			choice = "paper";
		}
		return "Computer plays " + choice + " and the " + winner + " won";
	}
}
